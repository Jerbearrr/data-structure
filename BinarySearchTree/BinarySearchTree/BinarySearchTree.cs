﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Node
    {
        public int item;
        public Node left;
        public Node right;
        public void Display()
        {
            Console.Write("[");
            Console.Write(item);
            Console.Write("]");
        }
    }
    class Tree
    {
        public Node root;
        public Tree()
        {
            root = null;
        }
        public Node ReturnRoot()
        {
            return root;
        }
        public void Insert(int id)
        {
            Node newNode = new Node();
            newNode.item = id;
            if (root == null)
                root = newNode;
            else
            {
                Node current = root;
                Node parent;
                while (true)
                {
                    parent = current;
                    if (id < current.item)
                    {
                        current = current.left;
                        if (current == null)
                        {
                            parent.left = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.right;
                        if (current == null)
                        {
                            parent.right = newNode;
                            return;
                        }
                    }
                }
            }
        }
        public void Preorder(Node Root)
        {
            if (Root != null)
            {
                Console.Write(Root.item + " ");
                Preorder(Root.left);
                Preorder(Root.right);
            }
        }
        public void Inorder(Node Root)
        {
            if (Root != null)
            {
                Inorder(Root.left);
                Console.Write(Root.item + " ");
                Inorder(Root.right);
            }
        }
        public void Postorder(Node Root)
        {
            if (Root != null)
            {
                Postorder(Root.left);
                Postorder(Root.right);
                Console.Write(Root.item + " ");
            }
        }
        public void Display(Node ptr, int level)
        {
            int i;
            if (ptr != null)
            {
                Display(ptr.right, level + 1);
                Console.WriteLine();
                if (ptr == root)
                    Console.Write("Root->:  ");
                else
                {
                    for (i = 0; i < level; i++)
                        Console.Write("       ");
                }
                Console.Write(ptr.item);
                Display(ptr.left, level + 1);
            }
        }
        public void Display()
        {
            Display(root, 1);
        }
    }
   
    public class BinarySearchTree
    {
        public static void Main()
        {            
            Console.Clear();
            Console.Title = "Binary Search Tree";
            int num;
            Tree BST = new Tree();
            Queue<int> list = new Queue<int>();

            start:
            Console.WriteLine("How many inputs?");            
            var count = Console.ReadLine();
            bool parseSuccess = int.TryParse(count, out num);            
            if(parseSuccess && num>0)
            {                
                for(int i = 1;i<=num;i++)
                {                    
                    int num2;
                    two:
                    Console.Write("Input a number: ");
                    count = Console.ReadLine();
                    parseSuccess = int.TryParse(count, out num2);                    
                    if(parseSuccess)
                    {
                        BST.Insert(num2);
                        list.Enqueue(num2);
                    }
                    else
                    {
                        Console.WriteLine("WRONG INPUT.PRESS ANY KEY TO INPUT AGAIN...");
                        Console.ReadKey();                        
                        goto two;
                    }
                }
            }
            else
            {
                Console.WriteLine("WRONG INPUT.PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                goto start;
            }
            Console.Clear();
            Console.Write("Inputs: ");
            foreach(int i in list)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();
            Console.WriteLine("Inorder Traversal : ");
            BST.Inorder(BST.ReturnRoot());
            Console.WriteLine(" ");
            Console.WriteLine();
            Console.WriteLine("Preorder Traversal : ");
            BST.Preorder(BST.ReturnRoot());
            Console.WriteLine(" ");
            Console.WriteLine();
            Console.WriteLine("Postorder Traversal : ");
            BST.Postorder(BST.ReturnRoot());
            Console.WriteLine(" ");
            BST.Display();
            Console.ReadLine();
        }
    }
}
