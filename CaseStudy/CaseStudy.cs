﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace CaseStudy
{
    class CaseStudy
    {              
        static void Main(string[] args)
        {
            string horizontalBorder = "====================";
            int choice;

            Console.Title = "DATA STRUCTURE";
            Console.ForegroundColor = ConsoleColor.DarkGreen;

        start:
            Console.Clear();            
            Console.WriteLine("Choose an option:\n{0}\n1.Stacks\n2.Queue\n3.Recursion\n4.Binary Tree\n5.Linked List\n6.Sorting\n{0}",horizontalBorder);
            
            choice = int.Parse(Console.ReadLine());
            switch(choice)
            {
                case 1:                    
                    Stacks.Stacks.Main();
                    break;
                case 2:
                    Queue.Queue.Main();
                    break;
                case 3:
                    Recursion.Recursion.Main();
                    break;
                case 4:
                    BinarySearchTree.BinarySearchTree.Main();
                    break;
                case 5:;
                    LinkedList.LinkedList.Main();
                    break;
                case 6:
                    Sorting.Sorting.Main();
                    break;
                default:
                    Console.WriteLine("INPUT ERROR.PRESS ANY KEY TO GO BACK.");
                    Console.ReadKey();
                    Console.Clear();
                    goto start;                   
            }
            Console.WriteLine("PRESS ANY KEY TO GO BACK TO MAIN MENU OR PRESS ESC TO EXIT APPLICATION");
            
            if(Console.ReadKey().Key == ConsoleKey.Escape)
            {
                Environment.Exit(0);
            }
            goto start;
            
        }
    }
}
