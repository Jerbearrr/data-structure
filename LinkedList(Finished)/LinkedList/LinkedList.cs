﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    public class LinkedList
    {
        public static void Main()
        {
            LinkedList<string> nameList = new LinkedList<string>();
            List<string> orderedList = new List<string>();
            string fullName;
            int numOfNames, ansNum, number;

            Console.WriteLine("LINKED LIST OF STUDENT NAMES:");
            start:
            Console.Write("How many names to put in a list? ");
            var nameNum = Console.ReadLine();
           while(!int.TryParse(nameNum,out numOfNames))
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                Console.Write("How many names to put in a list? ");
                nameNum = Console.ReadLine();
            }
           if(numOfNames<=0)
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                goto start;
            }

            Console.WriteLine();
            
            for(int i =0;i<numOfNames;i++)
            {

                Console.Write("Input name (LASTNAME, FIRSTNAME MIDDLENAME):");
                fullName = Console.ReadLine();
                while(int.TryParse(fullName,out int result))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.Write("Input name (LASTNAME, FIRSTNAME MIDDLENAME):");
                    fullName = Console.ReadLine();
                }

                while(fullName=="")
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.Write("Input name (LASTNAME, FIRSTNAME MIDDLENAME):");
                    fullName = Console.ReadLine();
                }
                if (nameList.First == null)
                {

                    nameList.AddFirst(fullName);

                }
                else
                {

                    nameList.AddLast(fullName);

                }
            }

            Console.Clear();

            do
            {
                
                Console.WriteLine("What do you want to do with the list? ");
                Console.WriteLine("1. Insert name\n2. Delete name\n3. Display List\n");
                var numAns = Console.ReadLine();
                while (!int.TryParse(numAns, out ansNum))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("What do you want to do with the list? ");
                    Console.WriteLine("1. Insert name\n2. Delete name\n3. Display List\n");
                    numAns = Console.ReadLine();
                }

                switch (ansNum)
                {

                    case 1:
                        //Insert Name
                        Console.Clear();
                        Console.Write("Input name to be inserted (LASTNAME, FIRSTNAME MIDDLENAME): ");
                        fullName = Console.ReadLine();
                        while (int.TryParse(fullName, out int result))
                        {
                            Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.Write("Input name (LASTNAME, FIRSTNAME MIDDLENAME):");
                            fullName = Console.ReadLine();
                        }

                        while (fullName == "")
                        {
                            Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.Write("Input name (LASTNAME, FIRSTNAME MIDDLENAME):");
                            fullName = Console.ReadLine();
                        }

                        fullName = fullName.ToUpper();

                        Console.WriteLine("INSERTED...");

                        nameList.AddLast(fullName);
                        orderedList = nameList.ToList<string>();
                        orderedList.Sort();                        
                        break;

                    case 2:
                        //Delete Name
                        Console.Clear();
                        Console.Write("Input name to be removed (LASTNAME, FIRSTNAME MIDDLENAME): ");
                        fullName = Console.ReadLine();
                        while (int.TryParse(fullName, out int result))
                        {
                            Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.Write("Input name to be removed (LASTNAME, FIRSTNAME MIDDLENAME): ");
                            fullName = Console.ReadLine();
                        }

                        while (fullName == "")
                        {
                            Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                            Console.ReadKey();
                            Console.Clear();
                            Console.Write("Input name to be removed (LASTNAME, FIRSTNAME MIDDLENAME): ");
                            fullName = Console.ReadLine();
                        }

                        fullName = fullName.ToUpper();

                        if (nameList.Contains(fullName))
                        {

                            nameList.Remove(fullName);
                            Console.WriteLine("DELETED...");

                        }
                        else
                        {

                            Console.WriteLine("NAME DOES NOT EXIST. PRESS ANY KEY TO INPUT AGAIN...");
                            Console.ReadKey();                            
                            goto case 2;
                        }
                        
                        orderedList = nameList.ToList<string>();
                        orderedList.Sort();
                        break;

                    case 3:
                        //Display List   
                        Console.Clear();
                        Console.WriteLine("LINKED LIST OF STUDENT NAMES:");
                        orderedList = nameList.ToList<string>();
                        orderedList.Sort();

                        foreach (string name in orderedList)
                        {

                            Console.WriteLine("{0}\n", name);

                        }
                        break;

                    default:

                        Console.Clear();
                        Console.WriteLine("WRONG INPUT");                        
                        break;
                }

                Console.WriteLine("Another modification?( 1=YES, 0=NO )");
                var yesOrNo = Console.ReadLine();
                while (!int.TryParse(yesOrNo, out number))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Another modification?( 1=YES, 0=NO )");
                    yesOrNo = Console.ReadLine();
                }
                Console.Clear();

                switch(number)
                {
                    case 1:
                        ansNum = 4;
                        break;
                    case 0:

                        foreach (string name in orderedList)
                        {

                            Console.WriteLine("{0}\n", name);

                        }
                        break;
                    default:

                        Console.Clear();
                        Console.WriteLine("WRONG INPUT...");
                        Console.WriteLine();
                        ansNum = 4;
                        break;

                }
                
            } while (ansNum> 3);
           
            Console.ReadKey();
        }        
    }
}
