﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    public class Queue
    {
        public static void Main()
        {
            string border = "=====", horizontalBorder = "=========";
            Queue<int> carPark = new Queue<int>();
            int ansNum, numOfCar;
            int[] entrance = new int[10];
            int[] exit = new int[10];
            bool isParkingEmpty = true;

            Console.Title = "QUEUE";

            start:
            do
            {                
                Console.Clear();
                Console.WriteLine("Will a car:\n{0}\n1. Enter\n2. Exit\n{0}", horizontalBorder);
                var numAns = Console.ReadLine();
                //Input Validation
                while(!int.TryParse(numAns,out ansNum))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Will a car:\n{0}\n1. Enter\n2. Exit\n{0}", horizontalBorder);
                    numAns = Console.ReadLine();
                }                

                switch (ansNum)
                {
                    case 1://Enter
                        do
                        {
                            Console.Clear();
                            Console.Write("Enter car number(1~10): ");
                            var carNum = Console.ReadLine();
                            //Input Validation
                            while (!int.TryParse(carNum, out numOfCar))
                            {
                                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                Console.ReadKey();
                                Console.Clear();
                                Console.Write("Enter car number(1~10): ");
                                carNum = Console.ReadLine();
                            }

                            if (numOfCar <= 0 || numOfCar>10)
                            {
                                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                Console.ReadKey();
                                goto case 1;
                            }

                            isParkingEmpty = false;

                            while (carPark.Contains(numOfCar))
                            {
                                Console.Clear();
                                Console.WriteLine("CAR IS ALREADY IN PARKING...");
                                Console.Write("Enter car number(1~10): ");
                                carNum = Console.ReadLine();
                                //Input Validation
                                while (!int.TryParse(carNum, out numOfCar))
                                {
                                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                    Console.ReadKey();
                                    Console.Clear();
                                    Console.Write("Enter car number(1~10): ");
                                    carNum = Console.ReadLine();
                                }
                            }
                            
                        } while (numOfCar > 10 || numOfCar <= 0);

                        //puts car in parking
                        carPark.Enqueue(numOfCar);
                        entrance[numOfCar - 1]++;                        
                        break;

                    case 2:
                        if (isParkingEmpty)
                        {
                            Console.WriteLine("PARKING IS EMPTY. PRESS ANY KEY TO GO BACK.");
                            Console.ReadKey();
                            goto start;
                        }
                        else
                        {
                            do
                            {
                                Console.Clear();
                                Console.Write("Enter car number(1~10): ");
                                var carNum = Console.ReadLine();
                                //Input Validation
                                while (!int.TryParse(carNum, out numOfCar))
                                {
                                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                    Console.ReadKey();
                                    Console.Clear();
                                    Console.Write("Enter car number(1~10): ");
                                    carNum = Console.ReadLine();
                                }

                                if (numOfCar <= 0||numOfCar>10)
                                {
                                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                    Console.ReadKey();
                                    goto case 2;
                                }                                                                    
                               
                                //executed if input is not in parking
                                while (!carPark.Contains(numOfCar))
                                {
                                    Console.Clear();
                                    Console.WriteLine("CAR IS NOT IN PARKING...");
                                    Console.Write("Enter car number(1~10): ");
                                    carNum = Console.ReadLine();
                                    //Input Validation
                                    while (!int.TryParse(carNum, out numOfCar))
                                    {
                                        Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                        Console.ReadKey();
                                        Console.Clear();
                                        Console.Write("Enter car number(1~10): ");
                                        carNum = Console.ReadLine();
                                    }
                                }

                                if (carPark.Peek() != numOfCar)
                                {
                                    Queue<int> temp = new Queue<int>();
                                    //executed while input is not removed from parking
                                    while (carPark.Peek() != numOfCar)
                                    {


                                        exit[carPark.Peek() - 1]++;
                                        entrance[carPark.Peek() - 1]++;
                                        temp.Enqueue(carPark.Dequeue());
                                    }
                                    carPark.Dequeue();
                                    foreach (int i in temp)
                                    {
                                        carPark.Enqueue(i);
                                    }
                                }
                                else
                                {
                                    carPark.Dequeue();
                                }

                                exit[numOfCar - 1]++;

                            } while (numOfCar > 10 || numOfCar < 0);
                        }
                        
                        break;

                    default:
                        Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO GO BACK.");
                        Console.ReadKey();
                        goto start;                        
                }
                //display parking
                Console.WriteLine("\nParking:\n{0}", border);
                foreach (int i in carPark)
                {
                    Console.WriteLine("{0}\n{1}", i, border);
                }

            retry:
                Console.WriteLine("Will a car enter/exit again (1=yes,0=no)?");
                numAns = Console.ReadLine();
                //Input Validation
                while (!int.TryParse(numAns, out ansNum))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Will a car enter/exit again (1=yes,0=no)?");
                    numAns = Console.ReadLine();
                }
                if (ansNum > 1 || ansNum < 0)
                {
                    Console.Clear();
                    goto retry;
                }
            } while (ansNum == 1);

            //display parking
            Console.WriteLine("\nParking:\n{0}", border);
            foreach (int i in carPark)
            {
                Console.WriteLine("{0}\n{1}", i, border);
            }
            Console.WriteLine("Number of entrance and exits:\n");

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Car {0}:\nEntrance: {1}\nExit: {2}\n", i + 1, entrance[i], exit[i]);
            }
            Console.ReadKey();           
        }
    }
}
