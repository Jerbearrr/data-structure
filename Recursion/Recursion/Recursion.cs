﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursion
{
    public class Recursion
    {
        public static void Main()
        {
            Console.Title = "RECURSION";
            Console.Clear();

            string horizontalBorder = "====================";
            int inputNum, num;
            bool parseSuccess;

            start:
            Console.WriteLine("Select:\n{0}\n1.Fibonacci\n2.Factorial\n3.Tower of Hanoi\n{0}", horizontalBorder);
            var input = Console.ReadLine();
            parseSuccess = int.TryParse(input, out inputNum);           
            if(parseSuccess)
            {
                if(inputNum<=0)
                {
                    Console.WriteLine("WRONG INPUT.PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    goto start;
                }
                else
                {
                    switch(inputNum)
                    {
                        case 1://Fibonacci
                            Console.Clear();

                            int n1 = 0, n2 = 1, n3, i, number;
                            Console.WriteLine("Enter the number of elements:\n");
                            var input2 = Console.ReadLine();
                            parseSuccess = int.TryParse(input2, out number);
                            if(parseSuccess)
                            {
                                if(number<=0)
                                {
                                    Console.WriteLine("WRONG INPUT.PRESS ANY KEY TO INPUT AGAIN...");
                                    Console.ReadKey();
                                    Console.Clear();
                                    goto case 1;
                                }
                                else
                                {
                                    Console.Write("Fibonacci Sequence: {0} {1} ", n1, n2); //printing 0 and 1    
                                    for (i = 2; i < number; ++i) //loop starts from 2 because 0 and 1 are already printed    
                                    {
                                        n3 = n1 + n2;
                                        Console.Write("{0} ", n3);
                                        n1 = n2;
                                        n2 = n3;
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("WRONG INPUT.PRESS ANY KEY TO INPUT AGAIN...");
                                Console.ReadKey();
                                Console.Clear();
                                goto case 1;
                            }                                                       
                                break;
                        case 2://Factorial
                            int fact = 1;
                            Console.Write("Enter any Number: ");
                            input2 = Console.ReadLine();
                            while(!int.TryParse(input2,out number))
                            {
                                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                Console.ReadKey();
                                Console.Clear();
                                Console.Write("Enter any Number: ");
                                input2 = Console.ReadLine();
                            }
                            for (i = 1; i <= number; i++)
                            {
                                fact *= i;
                            }
                            Console.Write("Factorial of " + number + " is: " + fact);

                            break;
                        case 3://Tower of Hanoi
                            Console.Clear();

                            TowerOfHanoi T = new TowerOfHanoi();
                            string cnumdiscs;
                            Console.Write("Enter the number of discs: ");
                            cnumdiscs = Console.ReadLine();
                            //Input Validation
                            while(!int.TryParse(cnumdiscs,out num))
                            {
                                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                                Console.ReadKey();
                                Console.Clear();
                                Console.Write("Enter the number of discs: ");
                                cnumdiscs = Console.ReadLine();
                            }
                    
                            T.numDiscs = Convert.ToInt32(num);
                            T.MoveTower(T.numDiscs, 1, 3, 2);
                            Console.ReadLine();                            
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                Console.WriteLine("WRONG INPUT.PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                goto start;
            }
            Console.ReadKey();
        }
    }
    class TowerOfHanoi
    {
        int m_numdiscs;
        public TowerOfHanoi()
        {
            numDiscs = 0;
        }
        public TowerOfHanoi(int newval)
        {
            numDiscs = newval;
        }
        public int numDiscs
        {
            get
            {
                return m_numdiscs;
            }
            set
            {
                if (value > 0)
                    m_numdiscs = value;
            }
        }
        public void MoveTower(int n, int from, int to, int other)
        {
            if (n > 0)
            {
                MoveTower(n - 1, from, other, to);
                Console.WriteLine("Move disk {0} from tower {1} to tower {2}",
                                   n, from, to);
                MoveTower(n - 1, other, to, from);
            }
        }
    }
}
