﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sorting;

namespace Sorting
{
    public class Sorting
    {
        public static void Main()
        {
            int result;
            start:
            do
            {            
                Console.WriteLine("Do you want to sort:\n1. Numbers\n2. Characters");
                var answer = Console.ReadLine();
                while (!int.TryParse(answer, out result))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Do you want to sort:\n1. Numbers\n2. Characters");
                    answer = Console.ReadLine();
                }
                Console.Clear();

                switch (result)
                {
                    case 1:
                        SortingInts();

                        break;

                    case 2:
                        SortingChars();
                        break;

                    default:
                        Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                        Console.ReadKey();
                        Console.Clear();
                        goto start;
                }

                Console.WriteLine("Do you want another sorting (1=yes,0=no)?");
                answer = Console.ReadLine();
                while (!int.TryParse(answer, out result))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Do you want another sorting (1=yes,0=no)?");
                    answer = Console.ReadLine();
                }
                Console.Clear();
            } while (result == 1);

            Console.ReadKey();
        }
        static void SortingInts()
        {            
            float  number;
            int result, numOfInput;

            start:
            Console.WriteLine("How many numbers to input?");
            var answer = Console.ReadLine();
            while (!int.TryParse(answer, out numOfInput))
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("How many numbers to input?");
                answer = Console.ReadLine();
            }
            if(numOfInput<=0)
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                goto start;
            }

            float[] numberList = new float[numOfInput];
            Console.Clear();

            for (int i = 0; i < numOfInput; i++)
            {
                Console.WriteLine("Input a number.");
                answer = Console.ReadLine();
                while (!float.TryParse(answer, out number))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Input a number.");
                    answer = Console.ReadLine();
                }
                numberList[i] = number;
            }

            Console.Clear();
            Console.Write("List of numbers(unsorted): ");

            //Display Unsorted
            foreach (float i in numberList)
            {
                Console.Write("{0} ", i);
            }

            Console.WriteLine();

            retry:
            Console.WriteLine("Select sorting method:\n1. Bubble Sorting\n2. Insertion Sorting");
            answer = Console.ReadLine(); 
            while(!int.TryParse(answer,out result))
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("Select sorting method:\n1. Bubble Sorting\n2. Insertion Sorting");
                answer = Console.ReadLine();
            }

            Console.Clear();

            switch (result)
            {
                //Bubble Sort
                case 1:
                    Console.WriteLine("Bubble Sorting:\n");
                    BubbleSortInts(numberList);
                    break;
                //Insertion Sort
                case 2:
                    Console.WriteLine("Insertion Sorting:\n");
                    InsertionSortInts(numberList);
                    break;
                default:
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    goto retry;
            }

            //Display Sorted            
            Console.WriteLine("SORTED...");
            Console.Write("\nList of numbers(sorted): ");
            foreach (float i in numberList)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine("\n");
        }

        static void SortingChars()
        {
            char letter;
            int result, numOfInput;

        start:
            Console.WriteLine("How many characters to input?");
            var answer = Console.ReadLine();
            while (!int.TryParse(answer, out numOfInput))
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("How many charactes to input?");
                answer = Console.ReadLine();
            }
            if (numOfInput <= 0)
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                goto start;
            }

            char[] charList = new char[numOfInput];
            Console.Clear();

            for (int i = 0; i < numOfInput; i++)
            {
                Console.WriteLine("Input a character.");
                answer = Console.ReadLine();
                while (!char.TryParse(answer, out letter))
                {
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    Console.WriteLine("Input a character.");
                    answer = Console.ReadLine();
                }                
                charList[i] = letter;
            }

            Console.Clear();
            Console.Write("List of numbers(unsorted): ");

            //Display Unsorted
            foreach (char i in charList)
            {
                Console.Write("{0} ", i);
            }

            Console.WriteLine();

        retry:
            Console.WriteLine("Select sorting method:\n1. Bubble Sorting\n2. Insertion Sorting");
            answer = Console.ReadLine();
            while (!int.TryParse(answer, out result))
            {
                Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("Select sorting method:\n1. Bubble Sorting\n2. Insertion Sorting");
                answer = Console.ReadLine();
            }

            Console.Clear();

            switch (result)
            {
                //Bubble Sort
                case 1:
                    Console.WriteLine("Bubble Sorting:\n");
                    BubbleSortChars(charList);
                    break;
                //Insertion Sort
                case 2:
                    Console.WriteLine("Insertion Sorting:\n");
                    InsertionSortChars(charList);
                    break;
                default:
                    Console.WriteLine("INPUT ERROR! PRESS ANY KEY TO INPUT AGAIN...");
                    Console.ReadKey();
                    Console.Clear();
                    goto retry;
            }

            //Display Sorted            
            Console.WriteLine("SORTED...");
            Console.Write("\nList of characters(sorted): ");
            foreach (char i in charList)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine("\n");
        }
        static void BubbleSortInts(float[] numberList)
        {
            Console.WriteLine("Sorting...");            
            for (int p = 0; p <= numberList.Length - 2; p++)
            {
                for (int i = 0; i <= numberList.Length - 2; i++)
                {
                    if (numberList[i] > numberList[i + 1])
                    {
                        float temp = numberList[i + 1];
                        numberList[i + 1] = numberList[i];
                        numberList[i] = temp;
                        //Display each steps                        
                        foreach(float j in numberList)
                        {
                            Console.Write("{0} ", j);                            
                        }
                        Console.WriteLine();
                    }                                        
                }                
            }                        

        }        
        static void InsertionSortInts(float[] numberList)
        {            
            for (int i = 1; i < numberList.Length; i++)
            {
                float val, flag;
                val = numberList[i];
                flag = 0;
                for (int j = i - 1; j >= 0 && flag != 1;)
                {
                    if (val < numberList[j])
                    {
                        numberList[j + 1] = numberList[j];
                        j--;
                        numberList[j + 1] = val;
                    }
                    else flag = 1;
                    //Display each steps                        
                    foreach (float k in numberList)
                    {
                        Console.Write("{0} ", k);
                    }
                    Console.WriteLine();
                }
                
            }            
        }

        static void BubbleSortChars(char[] charList)
        {            
            Console.WriteLine("Sorting...");
            for (int p = 0; p <= charList.Length - 2; p++)
            {
                for (int i = 0; i <= charList.Length - 2; i++)
                {
                    if (charList[i] > charList[i + 1])
                    {
                        char temp = charList[i + 1];
                        charList[i + 1] = charList[i];
                        charList[i] = temp;
                        //Display each steps                        
                        foreach (char j in charList)
                        {
                            Console.Write("{0} ", j);
                        }
                        Console.WriteLine();
                    }
                }
            }

        }
        static void InsertionSortChars(char[] charList)
        {
            for (int i = 1; i < charList.Length; i++)
            {
                char val;
                int flag = 0;
                val = charList[i];                
                for (int j = i - 1; j >= 0 && flag != 1;)
                {
                    if (val < charList[j])
                    {
                        charList[j + 1] = charList[j];
                        j--;
                        charList[j + 1] = val;
                    }
                    else flag = 1;
                    //Display each steps                        
                    foreach (char k in charList)
                    {
                        Console.Write("{0} ", k);
                    }
                    Console.WriteLine();
                }

            }
        }

    }
}
